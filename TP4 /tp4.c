#include <stdio.h>

int main() {
  int nombres[5];
  int i;
  int somme = 0;

  printf("Entrez 5 nombres entiers :\n");
  for (i = 0; i < 5; i++) {
    scanf("%d", &nombres[i]);
    somme += nombres[i];
  }

  printf("La somme des nombres est : %d\n", somme);

  return 0;
}
