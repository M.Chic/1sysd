#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int count_char(char *string, char c) {
    int count = 0;
    while (*string != '\0') {
        if (*string == c) {
            count++;
        }
        string++;
    }
    return count;
}
int count_words(char *string, char *word) {
    int count = 0;
    char *ptr = string;
    while ((ptr = strstr(ptr, word)) != NULL) {
            count++;
        ptr += strlen(word);
     }
    return count + 1;
 }
int count_words_better(char *string) {
    int count = 0;
    int in_word = 0;
    while (*string != '\0') {
        if (isspace(*string)) {
            in_word = 0;
    }
        else if (!in_word) {
            in_word = 1;
            count++;
           }
        string++;
       }
    return count;
    }

int main() {
    char chaine[50];
    char c;
    char mot[]=" ";

    printf("entrer une chaine de caract�re : ");
    scanf("%s", chaine);

    printf("Entrez un caractere : ");
    scanf(" %c", &c);

    int nombre_apparition = count_char(chaine, c);
    printf("\nLe caractere %c apparait %d fois dans la chaine.\n", c, nombre_apparition);

    int nombre_mots = count_words(chaine, mot);
    printf("la chaine \"%s\" contient %d mots \n",chaine,nombre_mots);

    return 0;
}
