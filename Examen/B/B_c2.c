#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int i;
void add_vectors(int n, double *t1, double *t2, long double *t3) {
    for ( i = 0; i < n; i++) {
        double diff = t1[i] - t2[i];
        t3[i] = diff * diff;
    }

}
double norm_vector(int n, double *t) {
    double sum_squares = 0;
    for (int i = 0; i < n; i++) {
        sum_squares += t[i] * t[i];
    }
    return sqrt(sum_squares);
}

int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	double T3[5];
	int n = 5;

	add_vectors(n,T1,T2,T3);

	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");

    printf("Tableau qui calculent les carr�s des diff�rences : \n");
    for (int i = 0; i < n; i++) {
        printf("%+.2lf ", T3[i]);
    }
    printf("\n");

    double norm_T1 = norm_vector(n, T1);
    printf("Norme euclidienne de T1 : %.2f\n", norm_T1);

    double norm_T2 = norm_vector(n, T2);
    printf("Norme euclidienne de T2 : %.2f\n", norm_T2);


	exit(EXIT_SUCCESS);
}


