#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int i;
void small_to_zero(int *t, int n, int val){
    for(i=0;i<n;i++){
        if(t[i]>val)
            t[i]=0;
    }
    }

int main()
{

    int t[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
    int n = sizeof(t) / sizeof(t[0]);
    int val = 5;
        printf("Tableau au debut  : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", t[i]);
    }
    printf("\n");

    small_to_zero(t, n, val);

    printf("Tableau a la fin  : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", t[i]);
    }
    printf("\n");


    return 0;
}
