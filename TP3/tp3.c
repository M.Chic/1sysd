#define bool int
#define false 0
#define true 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int lower = 1, upper = 100;
    srand(time(0));

    int secret = (rand() % (upper - lower + 1)) + lower;
    int n = 0;
    bool notfound = true;
    int guess;
    while (notfound) {
        printf ("Votre idée : \n");
        scanf("%d",&guess);
        n+=1;
        if (guess == secret) {
            notfound = false ;
        }
        if (guess > secret){
            printf("trop grand \n");
        }
        else {
            printf("trop petit \n");
        }
    }
    printf("gagné ! en %d Coups ! \n",n);
    printf("Bravo !");
    return 0 ;

}
